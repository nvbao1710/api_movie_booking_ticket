const express = require('express');
const { v1Router } = require('./v1');

const rootRouter = express.Router();
rootRouter.use('/v1', v1Router);

module.exports = rootRouter;
