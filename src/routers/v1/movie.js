const express = require('express');

const movieController = require('../../controllers/movie');
const { upload } = require('../../middlewares/upload');

const movieManagement = express.Router();

movieManagement.get('/banners', movieController.getBanners);

movieManagement.get('/', movieController.getMovies);
movieManagement.get('/search', movieController.searchMovies);
movieManagement.get('/search/date', movieController.getMovieByDate);

movieManagement.post(
  '/uploadPoster',
  upload.single('formFile'),
  movieController.uploadPoster,
);
movieManagement.post('/', movieController.createMovie);

movieManagement.put('/', movieController.editMovie);
movieManagement.patch('/', movieController.editMovie);

movieManagement.delete('/', movieController.deleteMovie);

module.exports = movieManagement;
