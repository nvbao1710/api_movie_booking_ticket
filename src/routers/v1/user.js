const express = require('express');

const userController = require('../../controllers/user');
const protectedRoute = require('../../middlewares/protectRoute');

const userManagement = express.Router();

userManagement.get('/roles', protectedRoute, userController.getRoleLists);
userManagement.get('/', userController.getUserLists);
userManagement.get('/search', userController.searchUser);
userManagement.put('/', protectedRoute, userController.editUser);

userManagement.post('/register', userController.register);
userManagement.post('/login', userController.login);
userManagement.post('/checkToken', userController.validateToken);

module.exports = userManagement;
