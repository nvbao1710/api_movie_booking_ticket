const { PrismaClient } = require('@prisma/client');
const bcrypt = require('bcryptjs');

const prisma = new PrismaClient();

const role = [
  {
    id: 'admin',
    role: 'admin',
  },
  {
    id: 'user',
    role: 'user',
  },
];

const user = [
  {
    name: 'Admin',
    email: 'admin@admin.com',
    phone: '0123456789',
    password: 'admin123',
    roleId: 'admin',
  },
  {
    name: 'User demo',
    email: 'user@email.com',
    phone: '0123456781',
    password: 'user123',
    roleId: 'user',
  },
];

const cineFlex = [{
  id: 1,
  name: 'CGV',
  logo: 'https://play-lh.googleusercontent.com/9N7f8PWb1zlDqOR4mepkNFkRt5SlrjFoLsg5jYtVhvq9LeQneLKyHg9eEx4BSgyl7F4',
}];

const cinema = [{
  id: 1,
  address: '72, Le Loi',
  name: 'CGV Le Loi',
  cineplexId: 1,
  image: 'https://www.cgv.vn/media/site/cache/1/980x415/b58515f018eb873dafa430b6f9ae0c1e/h/n/hnc-2.png',
}];
const movies = [
  {
    id: 2,
    name: 'Batman',
    startDate: '2022-06-09T00:00:00.000Z',
    time: '2022-04-09T00:00:00.000Z',
    evaluate: 5,
    poster: 'https://thegioidienanh.vn/stores/news_dataimages/anhvu/112021/08/12/2034_03.jpg?rt=20211108122124',
    trailer: 'https://www.youtube.com/watch?v=5iUVoG6UE9Q',
  },
  {
    id: 3,
    name: 'Thor and Thunder',
    startDate: '2022-04-09T16:11:30.000Z',
    time: '2022-04-09T16:11:30.000Z',
    evaluate: null,
    poster: '',
    trailer: '',
  },
  {
    id: 4,
    name: 'Thor 2',
    startDate: '2022-04-09T16:11:30.000Z',
    time: '2022-04-09T16:11:30.000Z',
    evaluate: null,
    poster: '',
    trailer: '',
  },
  {
    id: 5,
    name: 'Ironman 3',
    startDate: '2022-04-09T16:11:30.000Z',
    time: '2022-04-09T16:11:30.000Z',
    evaluate: null,
    poster: '',
    trailer: '',
  },
  {
    id: 6,
    name: 'Ironman 2',
    startDate: '2022-04-09T16:11:30.000Z',
    time: '2022-04-09T16:11:30.000Z',
    evaluate: 5,
    poster: '',
    trailer: '',
  },
];

const banners = [
  {
    banner: 'https://ephoto360.com/uploads/worigin/2019/07/06/banner_1-min5d2016cb2f52f_bf8e0aa1ab695fd74e3deb674eb1a2e0.jpg',
    movieId: 1,
  },
];
const showTime = [
  {
    id: 1,
    startTime: '2022-04-09T13:56:20.000Z',
    cinemaId: 1,
  },
  {
    id: 2,
    startTime: '2022-04-09T05:56:20.000Z',
    cinemaId: 1,
  },
  {
    id: 3,
    startTime: '2022-04-09T01:56:20.000Z',
    cinemaId: 1,
  },
  {
    id: 4,
    startTime: '2022-09-04T01:56:20.000Z',
    cinemaId: 1,
  },
];

const seats = [...Array(25).keys()].map((value) => ({
  name: `A${value}`,
  showTimeId: 1,
  status: true,
  price: 100,
}));

const main = async () => {
  console.log('Start seeding...');
  try {
    for (const item of role) {
      const result = await prisma.role.create({ data: item });
      console.log(`Created role with id: ${result.id}`);
    }

    const saltRounds = 10;
    for (const item of user) {
      const data = item;
      data.password = await bcrypt.hash(item.password, saltRounds);
      const result = await prisma.user.create({ data });
      console.log(`Created user with id: ${result.id}`);
    }

    for (const flex of cineFlex) {
      const createdflex = await prisma.cineflex.create({ data: flex });
      console.log(`Created cineplex: ${createdflex.id}`);
    }
    for (const cine of cinema) {
      const result = await prisma.cinema.create({ data: cine });
      console.log(`Created cinema: ${result.id}`);
    }
    for (const movie of movies) {
      const result = await prisma.movie.create({ data: movie });
      console.log(`Created movie: ${result.id}`);
    }
    for (const banner of banners) {
      const result = await prisma.banner.create({ data: banner });
      console.log(`Created movie: ${result.id}`);
    }
    for (const show of showTime) {
      const result = await prisma.showtime.create({ data: show });
      console.log(`Created movie: ${result.id}`);
    }
    for (const seat of seats) {
      const result = await prisma.seat.create({ data: seat });
      console.log(`Created movie: ${result.id}`);
    }
  } catch (e) {
    console.error(e);
  } finally {
    console.log('Seeding finished!');
  }
};

main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch((e) => {
    console.error(e);
    prisma.$disconnect()
      .then(() => {
        process.exit(1);
      });
  });
